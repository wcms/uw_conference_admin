/**
 * @file
 * Adds the ability to select a pre-existing location, making an AJAX call to populate the data from a central location.
 */

(function ($) {

  Drupal.behaviors.uw_conference_admin = {
    attach: function (context, settings) {
     if ($("#autofill-location-data").length === 0) {
      $locations = $('<select>').addClass('form-select').attr('id', 'autofill-location-data');

      // Create the list of locations.
      if (settings.uw_conference_admin.data) {
        // Add first location placeholder.
        $locations.append($('<option>').text(settings.uw_conference_admin.custom).attr('value', 'custom'));

        // Add location selections.
        $.each(settings.uw_conference_admin.data, function (key, value) {
          $locations.append($('<option>').text(value.name).attr('value', key));
        });

        // Add the list to the page.
        $locations.insertBefore('#' + settings.uw_conference_admin.containerId, context)
        $locations.wrap($('<div>').addClass('form-item'));
        $locations.before($('<label>').text(settings.uw_conference_admin.label).attr('for', settings.uw_conference_admin.selectId));
        $locations.after($('<div>').text(settings.uw_conference_admin.description).addClass('description'));

        // Set location data when the list changes.
        $locations.change(function () {
          if ($locations.val() != 'custom') {
            _populate_location_data(settings.uw_conference_admin.data[$locations.val()])
          }
          else {
            _populate_location_data(
              {
                name: '',
                additional: '',
                street: '',
                city: '',
                province: '',
                postal_code: '',
                country: 'ca',
                latitude: '',
                longitude: ''
              }
            );
          }
        });
      }
     }
    }
  };

  // TODO: use a variable setting from Drupal for the field name / language id selector.
  function _populate_location_data(data) {
    $('#edit-conference-location-name').val(data.name);
    $('#edit-conference-location-additional').val(data.additional);
    $('#edit-conference-location-street').val(data.street);
    $('#edit-conference-location-city').val(data.city);
    $('#edit-conference-location-province').val(data.province);
    $('#edit-conference-location-postalcode').val(data.postal_code);
    $('#edit-conference-location-country').val(data.country);
    $('#edit-conference-location-latitude').val(data.latitude);
    $('#edit-conference-location-longitude').val(data.longitude);
  }

}(jQuery));
