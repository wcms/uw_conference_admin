<?php

/**
 * @file
 * Code for agenda widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_agenda'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Agenda widget'),
  '#description'    => t('If enabled, this multi-day agenda is populated with the "Conference session" content type.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('agenda-widget')),
  '#suffix'       => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_agenda']['conference_agenda_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_agenda_title', ''),
  '#field_prefix'    => t('Optionally enter a title. The title appears at the top of the widget.'),
);

$conference_agenda_intro = variable_get('conference_agenda_intro', '');
if (is_array($conference_agenda_intro)) {
  $conference_agenda_intro = $conference_agenda_intro['value'];
}
$format = 'uw_tf_basic';

$form['conference_homepage']['conference_agenda']['conference_agenda_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_agenda_intro,
  '#field_prefix'    => t('Optionally enter an introduction. The introduction appears at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-agenda-intro-format { display: none; }</style>',
);
