<?php

/**
 * @file
 * Code for contact information-related fields in UW Conference Admin form.
 */

$form['conference']['conference_contact_information'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Contact information'),
  '#description'    => t('Enter optional contact information. If used, they will display as icons linked to the appropriate URL, right aligned, above the footer on every page.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
);

$form['conference']['conference_contact_information']['conference_contact_information_email'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Email'),
  '#default_value'  => variable_get('conference_contact_information_email', ''),
  '#field_prefix'    => t('Email address.'),
);

$form['conference']['conference_contact_information']['conference_social_handle'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter handle'),
  '#default_value'  => variable_get('conference_social_handle', ''),
  '#field_prefix'    => t('Twitter username for Twitter campaigns i.e. @UWaterloo.'),
);

$form['conference']['conference_contact_information']['conference_social_hashtag'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter hashtag'),
  '#default_value'  => variable_get('conference_social_hashtag', ''),
  '#field_prefix'    => t('Hashtag to use for Twitter campaigns i.e. #UWaterloo.'),
);

$form['conference']['conference_contact_information']['conference_social_twitter'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter URL'),
  '#default_value'  => variable_get('conference_social_twitter', ''),
  '#field_prefix'    => t('Twitter account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_facebook'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Facebook URL'),
  '#default_value'  => variable_get('conference_social_facebook', ''),
  '#field_prefix'    => t('Facebook account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_instagram'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Instagram URL'),
  '#default_value'  => variable_get('conference_social_instagram', ''),
  '#field_prefix'    => t('Instagram account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_linkedin'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Linkedin URL'),
  '#default_value'  => variable_get('conference_social_linkedin', ''),
  '#field_prefix'    => t('Linkedin account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_vimeo'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Vimeo URL'),
  '#default_value'  => variable_get('conference_social_vimeo', ''),
  '#field_prefix'    => t('Vimeo account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_youtube'] = array(
  '#type'           => 'textfield',
  '#title'          => t('YouTube URL'),
  '#default_value'  => variable_get('conference_social_youtube', ''),
  '#field_prefix'    => t('YouTube account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);
