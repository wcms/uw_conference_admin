<?php

/**
 * @file
 * Code for link widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_links'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Links widget'),
  '#description'    => t('If enabled, links populated with the "Conference link" content type display below an optional title and body as set below.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('links-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_links']['conference_links_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_links_title', ''),
  '#field_prefix'    => t('Optionally enter a title. It appears at the top of the widget.'),
);

$conference_links_intro = variable_get('conference_links_intro', '');
if (is_array($conference_links_intro)) {
  $conference_links_intro = $conference_links_intro['value'];
}

$term   = variable_get('conference_links_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_links']['conference_links_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_links_intro,
  '#field_prefix'    => t('Optionally enter an introduction. It appears at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-links-intro-format { display: none; }</style>',
);
