<?php

/**
 * @file
 * Code for content block widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_content_block'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Content block widget'),
  '#description'    => t('If enabled, a content block with title and body as set below will display on the homepage. This is useful for writing introductory text for your conference.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('content-block-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_content_block']['conference_content_block_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_content_block_title', ''),
  '#field_prefix'    => t('Optionally enter a title. The title appears above the body.'),
);

$conference_content_block_body = variable_get('conference_content_block_body', '');
if (is_array($conference_content_block_body)) {
  $conference_content_block_body = $conference_content_block_body['value'];
}

$term   = variable_get('conference_content_block');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_content_block']['conference_content_block_body'] = array(
  '#type'           => 'text_format',
  '#title'          => '<label>Body <abbr class="form-required">*<span>(required to use Content block widget)</span></abbr></label>',
  '#default_value'  => $conference_content_block_body,
  '#field_prefix'    => t('Enter the body.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-content-block-body-format { display: none; }</style>',
);
