<?php

/**
 * @file
 * Code for mailchimp widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_mailchimp_block'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('MailChimp widget'),
  '#description'    => t('If enabled, a MailChimp subscription form will display on the homepage (to set a MailChimp subscription form to show on every page use the "Conference settings – MailChimp settings").'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('mailchimp-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_mailchimp_block_title', ''),
  '#field_prefix'    => t('Optionally enter a title. The title will appear at the top of the widget.'),
);

$conference_mailchimp_block_body = variable_get('conference_mailchimp_block_body');
if (is_array($conference_mailchimp_block_body)) {
  $conference_mailchimp_block_body = $conference_mailchimp_block_body['value'];
}

$term   = variable_get('conference_mailchimp_block_body');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_body'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Body'),
  '#default_value'  => $conference_mailchimp_block_body,
  '#field_prefix'    => t('Optionally enter the body. The body will appear at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-mailchimp-block-body-format { display: none; }</style>',
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_action'] = array(
  '#type'             => 'textfield',
  '#title'            => '<label>MailChimp action <abbr class="form-required">*<span>(required to use MailChimp widget)</span></abbr></label>',
  '#default_value'    => variable_get('conference_mailchimp_block_action', ''),
  '#field_prefix'      => t('Form action. This should be a URL.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_user'] = array(
  '#type'             => 'textfield',
  '#title'            => '<label>User <abbr class="form-required">*<span>(required to use MailChimp widget)</span></abbr></label>',
  '#default_value'    => variable_get('conference_mailchimp_block_user', ''),
  '#field_prefix'      => t('User/account ID.'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_list'] = array(
  '#type'             => 'textfield',
  '#title'            => '<label>List <abbr class="form-required">*<span>(required to use MailChimp widget)</span></abbr></label>',
  '#default_value'    => variable_get('conference_mailchimp_block_list', ''),
  '#field_prefix'      => t('List ID.'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_submit_text'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Button text'),
  '#default_value'  => variable_get('conference_mailchimp_block_submit_text', ''),
  '#field_prefix'    => t('Optionally enter label text for the button. If left blank button text will be "Subscribe".'),
  '#maxlength'      => 12,
);
