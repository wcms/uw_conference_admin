<?php

/**
 * @file
 * Code for mailchimp-related fields in UW Conference Admin form.
 */

$form['conference']['conference_contact_information']['conference_mailchimp'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('MailChimp settings'),
  '#description'    => t('Use these MailChimp settings if you would like people to subscribe to a MailChimp newsletter. The subscription form will display, left aligned, above the footer on every page.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_mailchimp_title', ''),
  '#field_prefix'    => t('Title of the form.'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_submit_text'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Button text'),
  '#default_value'  => variable_get('conference_mailchimp_submit_text', ''),
  '#field_prefix'    => t('Optionally enter label text for the button. If left blank button text will be "Subscribe".'),
  '#maxlength'      => 12,
);

$conference_mailchimp_description = variable_get('conference_mailchimp_description', '');
if (is_array($conference_mailchimp_description)) {
  $conference_mailchimp_description = $conference_mailchimp_description['value'];
}

$term   = variable_get('conference_mailchimp_description');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_description'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Description'),
  '#default_value'  => $conference_mailchimp_description,
  '#field_prefix'    => t('Description of the form.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-mailchimp-description-format { display: none; }</style>',
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_action'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>MailChimp action <abbr class="form-required">*<span>(required to use Mailchimp)</span></abbr></label>',
  '#default_value'  => variable_get('conference_mailchimp_action', ''),
  '#field_prefix'    => t('Form action. This should be a URL.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_user'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>User <abbr class="form-required">*<span>(required to use Mailchimp)</span></abbr></label>',
  '#default_value'  => variable_get('conference_mailchimp_user', ''),
  '#field_prefix'    => t('User/account ID.'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_list'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>List <abbr class="form-required">*<span>(required to use Mailchimp)</span></abbr></label>',
  '#default_value'  => variable_get('conference_mailchimp_list', ''),
  '#field_prefix'    => t('List ID.'),
);
