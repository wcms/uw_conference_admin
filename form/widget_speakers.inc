<?php

/**
 * @file
 * Code for speakers widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_speakers'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Speakers widget'),
  '#description'    => t('If enabled, up to 4 speakers from the "Conference speaker" content type (marked as "Promoted to front page" in the "Publishing options") will display on the homepage.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('speakers-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_speakers']['conference_speakers_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_speakers_title', ''),
  '#field_prefix'    => t('Title to display above the speakers.'),
);

$conference_speakers_intro = variable_get('conference_speakers_intro');
if (is_array($conference_speakers_intro)) {
  $conference_speakers_intro = $conference_speakers_intro['value'];
}

$term   = variable_get('conference_speakers_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_speakers']['conference_speakers_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_speakers_intro,
  '#field_prefix'    => t('Introductory text to display above the speakers.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-speakers-intro-format { display: none; }</style>',
);
