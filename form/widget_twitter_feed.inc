<?php

/**
 * @file
 * Code for twitter feed widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_twitter_feed'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Twitter feed widget'),
  '#description'    => t('If enabled, a Twitter feed will display on the homepage.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('twitter-feed-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_feed_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_twitter_feed_title', ''),
  '#field_prefix'    => t('The title to appear above the tweets.'),
);

$conference_twitter_feed_intro = variable_get('conference_twitter_feed_intro', '');
if (is_array($conference_twitter_feed_intro)) {
  $conference_twitter_feed_intro = $conference_twitter_feed_intro['value'];
}

$term   = variable_get('conference_twitter_feed_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_feed_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_twitter_feed_intro,
  '#field_prefix'    => t('Introductory text that appears above the tweets.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-twitter-feed-intro-format { display: none; }</style>',
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_oauth_access_token'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>OAuth access token <abbr class="form-required">*<span>(required to use Twitter feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_twitter_oauth_access_token', ''),
  '#field_prefix'    => t('OAuth access token for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_oauth_access_token_secret'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>OAuth access token secret <abbr class="form-required">*<span>(required to use Twitter feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_twitter_oauth_access_token_secret', ''),
  '#field_prefix'    => t('OAuth access token secret for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_consumer_key'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>Consumer key <abbr class="form-required">*<span>(required to use Twitter feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_twitter_consumer_key', ''),
  '#field_prefix'    => t('Consumer key for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_consumer_secret'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>Consumer secret <abbr class="form-required">*<span>(required to use Twitter feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_twitter_consumer_secret', ''),
  '#field_prefix'    => t('Consumer secret for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_query_string'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>Query string <abbr class="form-required">*<span>(required to use Twitter feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_twitter_query_string', ''),
  '#field_prefix'    => t('Search string for Twitter feed. E.g. #wins2015'),
);
