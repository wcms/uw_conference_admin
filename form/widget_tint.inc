<?php

/**
 * @file
 * Code for tint widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_tint'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Tint feed widget'),
  '#description'    => t('If enabled, a Tint feed will display on the homepage.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('tint-feed-widget')),
  '#suffix'         => '<a href="#main-content" class="back-to-top">Back to top</a>',
);

$form['conference_homepage']['conference_tint']['conference_tint_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_tint_title', ''),
  '#field_prefix'    => t('Title to display above the tint feed.'),
);

$conference_tint_intro = variable_get('conference_tint_intro', '');
if (is_array($conference_tint_intro)) {
  $conference_tint_intro = $conference_tint_intro['value'];
}

$term   = variable_get('conference_tint_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_tint']['conference_tint_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_tint_intro,
  '#field_prefix'    => t('Introductory text to display above the tint feed.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-tint-intro-format { display: none; }</style>',
);

$form['conference_homepage']['conference_tint']['conference_tint_id'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>ID <abbr class="form-required">*<span>(required to use Tint feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_tint_id', ''),
  '#field_prefix'    => t('The ID of the tint feed.'),
);

$form['conference_homepage']['conference_tint']['conference_tint_class'] = array(
  '#type'           => 'textfield',
  '#title'          => '<label>Class <abbr class="form-required">*<span>(required to use Tint feed widget)</span></abbr></label>',
  '#default_value'  => variable_get('conference_tint_class', ''),
  '#field_prefix'    => t('The CSS classname. This is provided by Tint.'),
);

$form['conference_homepage']['conference_tint']['conference_tint_columns'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Columns'),
  '#default_value'  => variable_get('conference_tint_columns', ''),
  '#field_prefix'    => t('The number of columns. Leave blank to let Tint decide what is best.'),
);

$form['conference_homepage']['conference_tint']['conference_tint_width'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Width'),
  '#default_value'  => variable_get('conference_tint_width', ''),
  '#field_prefix'    => t('The width. Include units. E.g. 960px or 100%.'),
);

$form['conference_homepage']['conference_tint']['conference_tint_height'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Height'),
  '#default_value'  => variable_get('conference_tint_height', ''),
  '#field_prefix'    => t('The height. Include units. E.g. 960px or 100%.'),
);
